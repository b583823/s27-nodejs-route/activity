let http = require("http");
const port = 4000;

http.createServer((request,response) =>{

	if(request.url == "/" && request.method == "GET"){

			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Welcome to booking system.");
	}
	if(request.url == "/profile" && request.method == "GET"){

			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Welcome to our profile.");
	}
	if(request.url == "/course" && request.method == "GET"){

			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Here's our courses available.");
	}
	if(request.url == "/addCourse" && request.method == "POST"){

			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Add course to our resources.");
	}
	if(request.url == "/updateCourse" && request.method == "PUT"){

			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Update a course to our resources.");
	}
	if(request.url == "/archiveCourse" && request.method == "DELETE"){

			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("Archive course to our resources.");
	}

}).listen(port);
console.log(`Server is runnit at localHost:${port}`);